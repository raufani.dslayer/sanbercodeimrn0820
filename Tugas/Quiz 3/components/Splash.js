import React, { Component } from 'react';
import { StackNavigator, withNavigation } from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';
import home from './Home';

//const routes = {
//  home: { screen: home },
//};

export default class Login extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageBackground}>
          <Image
            style={styles.image}
            source={require('../assets/sanber_banner.png')}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

/*------------------------------*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: 'white',
    padding: 10,
  },
  imageBackground:{
    height: 160,
    width: 160,
    backgroundColor: 'grey',
    borderRadius: 80,
    paddingTop: 40,
    alignSelf: 'center'
  },
  image: {
    width: 150,
    height: 80,
    alignSelf: 'center',
  },
});
