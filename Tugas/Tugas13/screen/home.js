import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, Image } from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';


export default class home extends Component {
  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.paragraphMedium}>
          Tentang Saya
        </Text>
        <Image
          style={[styles.image, {marginBottom: 10}]}
          source={require('/assets/photo.jpg')}
          resizeMode='contain'
        />
        <Text style={{
          textAlign: 'center', padding: '10'
        }}>
          Raufani Aminullah Aziz
        </Text>
        <Card style={styles.card}>
          <Text style={{
            textAlign: 'center', padding: '10', fontWeight: 'bold'
          }}>
            Portofolio
          </Text>
          <View
          style={{
            borderBottomWidth: 1, paddingTop: 5, marginBottom: 5
          }}></View>
          <Image
            style={{width: 48, height: 48, alignSelf: 'center'}}
            source={require('/assets/git.png')}
            resizeMode='contain'
          />
          <Text style={{
            textAlign: 'center', padding: '10'
          }}>
            @raufani.dslayer
          </Text>
        </Card>
        <Card style={styles.card}>
          <Text style={{
            textAlign: 'center', padding: '10', fontWeight: 'bold'
          }}>
            Hubungi Saya
          </Text>
          <View
          style={{
            borderBottomWidth: 1, paddingTop: 5, marginBottom: 5
          }}></View>
          <Image
            style={{width: 48, height: 48, alignSelf: 'center'}}
            source={require('/assets/fb.png')}
            resizeMode='contain'
          />
          <Text style={{
            textAlign: 'center', padding: '10'
          }}>
            @raufani
          </Text>
        </Card>
        
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 10
  },
  paragraphMedium: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    borderColor: '#d500f9',
    padding: 5,
  },
  card:{
    borderRadius : 15,
    padding: 10,
    margin: 20
  },
  image: {
    width: 300,
    height: 100,
    alignSelf: 'center'
  },
});
