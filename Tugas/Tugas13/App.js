import React, { Component } from 'react';
import { StackNavigator, withNavigation } from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';
import home from './screen/home.js';

const routes = {
  home: { screen: home },
};

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('/assets/sanber_banner.png')}
          resizeMode="contain"
        />

        <Text style={styles.paragraphMedium}>Login</Text>

        <Card style={styles.card}>
          <Text>Username</Text>

          <UselessTextInputMultiline />

          <Text>Password</Text>
          <UselessTextInputMultiline />
        </Card>

        <View style={styles.columnView_1}>
          <TouchableOpacity
            style={[styles.buttonShape_1, styles.color_1,]}
            activeOpacity = { .5 }
            onPress={() => this.props.navigation.navigate('home') }>
              <Text style={styles.TextStyle}> Masuk </Text>
          </TouchableOpacity>
          <Text style={{textAlign: "center", fontWeight: "bold"}}> or </Text>
          <TouchableOpacity
            style={[styles.buttonShape_1, styles.color_2,]}
            activeOpacity = { .5 }
            onPress={() => this.props.navigation.navigate('home') }>
              <Text style={styles.TextStyle}> Daftar </Text>
          </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}

/*------------------------------*/
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      maxLength={10}
      style={styles.textInput}
      onChangeText={(text) => this.setState({ text })}
    />
  );
}

const UselessTextInputMultiline = () => {
  const [value, onChangeText] = React.useState('');

  return (
    <View
      style={{
        backgroundColor: value,
        borderBottomColor: '#d500f9',
        borderColor: '#d500f9',
        marginTop: 10,
        marginBottom: 15,
        paddingRight: 10,
        paddingLeft: 10
      }}>
      <UselessTextInput
        multiline
        numberOfLines={2}
        onChangeText={(text) => onChangeText(text)}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 10,
  },
  paragraphMedium: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    borderColor: '#d500f9',
    padding: 5,
  },
  card: {
    borderRadius: 15,
    padding: 10,
    margin: 20,
  },
  columnView_1: {
    justify: 'center',
    alignSelf: 'center',
    borderRadius: 15,
  },
  image: {
    width: 300,
    height: 100,
    alignSelf: 'center',
  },
  textInput: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 25,
    borderColor: 'gray',
    borderRadius: 16,
    borderWidth: 1,
  },
  color_1:{
    backgroundColor:'#00BCD4',
  },
  color_2:{
    backgroundColor:'#03254c',
  },
  buttonShape_1: {
    marginTop: 5,
    marginBottom: 5,
    paddingTop:10,
    paddingBottom:10,
    marginLeft:30,
    marginRight:30,
    
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#fff',
    width: 100,
    height: 40
  },
  TextStyle:{
      color:'#fff',
      textAlign:'center',
  }
});
