import React, { Component } from 'react';

import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';





export default class AddScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.paragraphMedium}>Tentang Saya</Text>
        
        
      </View>
    );
  }
}

/*------------------------------*/


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 10,
  },
  paragraphMedium: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    borderColor: '#d500f9',
    padding: 5,
  },
  card: {
    borderRadius: 15,
    padding: 10,
    margin: 20,
  },
  columnView_1: {
    justify: 'center',
    alignSelf: 'center',
    borderRadius: 15,
  },
  image: {
    width: 300,
    height: 100,
    alignSelf: 'center',
  },
  textInput: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 25,
    borderColor: 'gray',
    borderRadius: 16,
    borderWidth: 1,
  },
  color_1:{
    backgroundColor:'#00BCD4',
  },
  color_2:{
    backgroundColor:'#03254c',
  },
  buttonShape_1: {
    marginTop: 5,
    marginBottom: 5,
    paddingTop:10,
    paddingBottom:10,
    marginLeft:30,
    marginRight:30,
    
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#fff',
    width: 100,
    height: 40
  },
  TextStyle:{
      color:'#fff',
      textAlign:'center',
  }
});
